var express= require('express')
var app=express()
let port = process.env.PORT || 3000
app.get('/',function(req,res) {
    res.send('<h1>Hello World</h1>')
})
app.get('/weather',function(req,res)
{
    res.send({'forecast' : 'snowy',
              'location' : 'boston'})
})
app.listen(port, function(err){
    if(err) throw err
    console.log('server running on port'+ port)
})
